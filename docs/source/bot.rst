bot package
===========

.. toctree::

    bot.callbackqueries
    bot.constants
    bot.commands
    bot.errors
    bot.inlinequeries
    bot.jobs
    bot.meta
    bot.setup
    bot.utils
