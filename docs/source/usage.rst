Befehle
-------

Die folgenden Befehle zeigen jeweils das `nächste` verfügbare Menü für die entsprechende Mensa an:

- ``/m1``: `Mensa 1`_
- ``/m2``: `Mensa 2`_
- ``/cm2``: `Cafeteria Mensa 2`_
- ``/360``: `360 Grad`_
- ``/b4u``: `bistro4u`_
- ``/kk``: `Kantina Kreativa`_
- ``/truck``: `Food Truck TU BS`_

.. _`Mensa 1`: https://www.stw-on.de/braunschweig/essen/mensen-cafeterien/mensa-1/
.. _`Mensa 2`: https://www.stw-on.de/braunschweig/essen/mensen-cafeterien/mensa-2/
.. _`Cafeteria Mensa 2`: https://www.stw-on.de/braunschweig/essen/mensen-cafeterien/cafeteria-mensa-2/
.. _`360 Grad`: https://www.stw-on.de/braunschweig/essen/mensen-cafeterien/kantina-kreativa/
.. _`bistro4u`: https://www.stw-on.de/braunschweig/essen/mensen-cafeterien/360/
.. _`Kantina Kreativa`: https://www.stw-on.de/braunschweig/essen/mensen-cafeterien/bistro4u/
.. _`Food Truck TU BS`: https://www.stw-on.de/braunschweig/essen/mensen-cafeterien/foodtruck/

Knöpfe
------

Unter jedem Menü hängen mehrere Knöpfe:

- ``Aktualisieren 🔄`` zeigt das `nächste` verfügbare Menü für die gerade ausgewählte Mensa an.
- ``Online-Speiseplan 🍽`` führt zur offiziellen Website mit dem Speiseplan der gerade ausgewählten Mensa.
- ``Weniger Info 🔼``/``Mehr Info 🔽`` zeigt Allergene und Zusatzstoffe des aktuell ausgewählten Menüs an oder blendet sie aus.
- ``Legende 📜`` zeigt eine Legende der Symbole des aktuell ausgewählten Menüs an.
- ``⬅️...``/``...➡️`` zeigt das `nächste` verfügbare Menü der entsprechenden Mensa an.

Inline-Modus
------------

Der Bot kann auch im Inline-Modus benutzt werden, d.h. in einem beliebigen Chat kann man::

    @tubs_mensa_bot <suchworte>

eintippen, um nach bestimmten Menüs zu suchen. Folgende Suchworte werden verstanden:

* Wochentage (oder Abkürzungen), z.B. ``Montag``, ``di``, ``Mitt``
* Namen der Mensen oder Teile davon, z.B. ``Kantine``, ``b4u``, ``Mensa 1``, ``m2``
* ``früh``, ``mittag``, ``abend``, um anzugeben ob ein Frühstuck, Mittagessen oder Abendbrot gesucht ist

Groß- und Kleinschreibung wird nicht berücksichtigt. Suchworte müssen mit Leerzeichen getrennt werden. Es können auch mehrere Mensen/Tage/Mahlzeiten angegeben werden.

Der Bot zeigt eine Liste mit gefundenen Menüs an, von denen Du durch Anklicken eine auswählen kannst. Wenn Du keine Suchwort oder keinen Tag angibt, werden für alle (ausgewählten) Mensen das jeweils `nächste` Menü angezeigt.

Sonstiges
---------

Der Befehl ``/hilfe`` zeigt eine kurze Übersicht über den Bot an und verlinkt auf relevante Websites.
