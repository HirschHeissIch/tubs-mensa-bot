#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""The script that runs the bot."""
import logging
from configparser import ConfigParser
from telegram import ParseMode
from telegram.ext import Updater, PicklePersistence, Defaults

from bot import setup_dispatcher

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO,
    filename='tmb.log',
)

logger = logging.getLogger(__name__)


def main() -> None:
    """Start the bot."""
    # Read configuration values from bot.ini
    config = ConfigParser()
    config.read('bot.ini')
    token = config['tubs-mensa-bot']['token']
    admin_id = int(config['tubs-mensa-bot']['admins_chat_id'])

    # Create the Updater and pass it your bot's token.
    defaults = Defaults(parse_mode=ParseMode.HTML)
    persistence = PicklePersistence('tmb_db', single_file=False)
    updater = Updater(token, persistence=persistence, defaults=defaults)

    # Register handlers and prepare data
    setup_dispatcher(updater.dispatcher, admin_id=admin_id)

    # Start the Bot
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
