#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Meta callbacks."""
from typing import cast

from telegram import Update, InlineKeyboardMarkup, InlineKeyboardButton, Message
from telegram.ext import CallbackContext

from .constants import USER_GUIDE


def info(update: Update, context: CallbackContext) -> None:
    """
    Returns some info about the bot.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    text = (
        'Ich bin der <b>TU BS Mensa Bot</b> und hier, damit Du immer weißt, was Du in den '
        'Mensen des Studentenwerks Osniedersachsen zum Essen findest 🍽. Um die '
        'nächste Mahlzeit zu finden, tippe einfach einen der folgenden Befehle ein:'
        '\n\n/m1 für die Mensa 1'
        '\n/m2 für die Mensa 2'
        '\n/360 für das 360 Grad'
        '\n/truck for den Food Truck'
        '\n/b4u für das bistro4u oder'
        '\n/kk für die Kantina Kreativa.'
        f'\n\nOder probiere den Inline-Modus: <code>@{context.bot.username} ...</code>'
        '\n\nFür mehr Details, besuche mich doch auf meiner Homepage 🙂.'
    )

    keyboard = InlineKeyboardMarkup.from_column(
        [
            InlineKeyboardButton('Anleitung 🤖', url=USER_GUIDE),
            InlineKeyboardButton(
                'Studentenwerk Ostniedersachsen', url='https://www.stw-on.de/braunschweig/essen/'
            ),
            InlineKeyboardButton('Inline-Modus', switch_inline_query_current_chat=''),
        ]
    )

    cast(Message, update.message).reply_text(text, reply_markup=keyboard)
