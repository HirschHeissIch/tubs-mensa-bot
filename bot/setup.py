#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# TODO: Remove this after upgrading PTB to v13.2  # pylint: disable=W0511
# type: ignore[arg-type]
"""Setup functions."""
import datetime as dt
from typing import cast

from telegram.ext import (
    Dispatcher,
    CommandHandler,
    CallbackQueryHandler,
    InlineQueryHandler,
    Filters,
)

from utils import SCDict
from .callbackqueries import (
    next_serving_callback_query,
    show_legend,
    toggle_verbosity,
)
from .constants import (
    ABBRVS,
    SHOW_NEXT_PATTERN,
    ADMIN_KEY,
    SHOW_LEGEND_PATTERN,
    TOGGLE_VERBOSITY_PATTERN,
    REFRESH_PATTERN,
    SERVING_COLLECTIONS,
    NO_MENU_PATTERN,
)
from .commands import next_menu_command
from .errors import error
from .jobs import update_menu
from .inlinequeries import servings_inline_query, answer_deep_linking
from .meta import info


def setup_dispatcher(dispatcher: Dispatcher, admin_id: int) -> None:
    """
    Adds handlers and sets up jobs. Convenience method to avoid doing that all in the main script.

    Args:
        dispatcher: The :class:`telegram.ext.Dispatcher`.
        admin_id: The admins chat id.
    """
    # Prepare data
    dispatcher.bot_data[ADMIN_KEY] = admin_id

    # error handler
    dispatcher.add_error_handler(error)

    # Fetch the menu once a day …
    job = dispatcher.job_queue.run_daily(update_menu, dt.time(3, 0, 0), name='fetch_menus_daily')
    # … and once right now
    job.run(dispatcher)

    # Callback Queries
    dispatcher.add_handler(
        CallbackQueryHandler(next_serving_callback_query, pattern=SHOW_NEXT_PATTERN)
    )
    dispatcher.add_handler(CallbackQueryHandler(show_legend, pattern=SHOW_LEGEND_PATTERN))
    dispatcher.add_handler(
        CallbackQueryHandler(toggle_verbosity, pattern=TOGGLE_VERBOSITY_PATTERN)
    )
    dispatcher.add_handler(
        CallbackQueryHandler(next_serving_callback_query, pattern=REFRESH_PATTERN)
    )

    # Inline Mode
    dispatcher.add_handler(InlineQueryHandler(servings_inline_query))
    # Must be before the usual start command!
    dispatcher.add_handler(
        CommandHandler('start', answer_deep_linking, filters=Filters.regex(NO_MENU_PATTERN))
    )

    # basic command handlers
    # Must be after the inline mode deep linking start command!
    dispatcher.add_handler(CommandHandler(['start', 'help', 'hilfe'], info))
    dispatcher.add_handler(CommandHandler(list(ABBRVS.keys()), next_menu_command))

    # Set bot commands
    servings_collection = cast(SCDict, dispatcher.bot_data[SERVING_COLLECTIONS])
    bot_commands = [
        (abbreviation, servings_collection[cid].location.name)
        for abbreviation, cid in ABBRVS.items()
    ]
    bot_commands.append(('hilfe', 'Allgemeine Info'))
    dispatcher.bot.set_my_commands(bot_commands)
