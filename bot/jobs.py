#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Job callbacks."""
from typing import cast

from telegram.ext import CallbackContext
from stwon import BRUNSWICK, ServingsCollection, get_meals, get_cafeteria
from utils import SCDict

from .constants import SERVING_COLLECTIONS


def update_menu(context: CallbackContext) -> None:
    """
    Fetches the menu from the web and stores it in :attr:`telegram.ext.Dispatcher.bot_data`.

    Args:
        context: The callback context as provided by the dispatcher.
    """
    serving_collections = cast(SCDict, context.bot_data.setdefault(SERVING_COLLECTIONS, {}))
    for cid in BRUNSWICK:
        serving_collection = serving_collections.setdefault(
            cid, ServingsCollection(get_cafeteria(cid))
        )
        announcements, meals = get_meals(cid)
        serving_collection.register_announcements(announcements)
        serving_collection.register_meals(meals)
