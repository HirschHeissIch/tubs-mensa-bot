#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Utility functions."""
import datetime as dt
from functools import wraps
from typing import (
    Union,
    Optional,
    Literal,
    cast,
    List,
    TypedDict,
    overload,
    TypeVar,
    Callable,
    Any,
)
from telegram import InlineKeyboardMarkup, InlineKeyboardButton, ChatAction, Update
from telegram import constants
from telegram.ext import CallbackContext
from stwon import next_cafeteria, ServingsCollection, Serving, Cafeteria
from utils import TimesLiteral

from .constants import (
    SERVING_COLLECTIONS,
    SHOW_NEXT_DATA,
    SHOW_LEGEND_DATA,
    TOGGLE_VERBOSITY_DATA,
    REFRESH_DATA,
)

SEND_TEXT: str = 'send_message'
"""
:obj:`str`: Convenience constant to use for the ``method`` argument of :meth:`next_menu_kwargs`.
"""
EDIT_TEXT: str = 'edit_message_text'
"""
:obj:`str`: Convenience constant to use for the ``method`` argument of :meth:`next_menu_kwargs`.
"""
ANSWER_INLINE: str = 'answer_inline_query'
"""
:obj:`str`: Convenience constant to use for the ``method`` argument of :meth:`next_menu_kwargs`.
"""
ALL_METHODS = [SEND_TEXT, EDIT_TEXT, ANSWER_INLINE]


class SendTextDict(TypedDict):  # pylint: disable=C0115,C0112
    """"""  # hide from docs

    text: str
    reply_markup: InlineKeyboardMarkup


class AnswerInlineDict(TypedDict):  # pylint: disable=C0115,C0112
    """"""  # hide from docs

    message_text: str
    reply_markup: InlineKeyboardMarkup


@overload
def next_serving_kwargs(
    method: Literal['send_message', 'edit_message_text'],
    context: CallbackContext,
    cid: int,
    from_dtm: Optional[dt.datetime] = None,
    times: List[TimesLiteral] = None,
    verbose: bool = False,
    navigation_keyboard: bool = True,
) -> SendTextDict:
    ...


@overload
def next_serving_kwargs(
    method: Literal['answer_inline_query'],
    context: CallbackContext,
    cid: int,
    from_dtm: Optional[dt.datetime] = None,
    times: List[TimesLiteral] = None,
    verbose: bool = False,
    navigation_keyboard: bool = True,
) -> AnswerInlineDict:
    ...


def next_serving_kwargs(  # pylint: disable=R0914
    method: Literal['answer_inline_query', 'send_message', 'edit_message_text'],
    context: CallbackContext,
    cid: int,
    from_dtm: Optional[dt.datetime] = None,
    times: List[TimesLiteral] = None,
    verbose: bool = False,
    navigation_keyboard: bool = True,
) -> Union[SendTextDict, AnswerInlineDict]:
    """
    Gives the keyword arguments needed to display the next menu of the selected cafeteria. Can be
    used with :attr:`telegram.Bot.send_message` :attr:`telegram.Bot.edit_message_text` and
    :attr:`telegram.Bot.answer_callback_query`:

    .. code:: python

        bot.send_message(chat_id=chat_id, **next_menu_kwargs('send_message', context, cid))
        bot.edit_message_text(
            chat_id=chat_id,
            message_id=message_id,
            **next_menu_kwargs('edit_message_text', context, cid)
        )
        inline_kwargs = next_menu_kwargs('answer_inline_query', context, cid)
        bot.answer_callback_query(
            InlineQueryResultArticle(
                id=id,
                title=title,
                input_message_content=InputTextMessageContent(inline_kwargs['message_text']),
                reply_markup=inline_kwargs['reply_markup'],
            )
        )

    Args:
        method: The method to produce the kwargs for. Use one of :attr:`SEND_TEXT`,
            :attr:`EDIT_TEXT` and :attr:`ANSWER_INLINE`.
        context: The callback context as provided by the dispatcher.
        cid: The ID of the selected cafeteria.
        from_dtm: If passed, will only consider servings available at or after that time.
            Naive objects will be assumed to be in :attr:`utils.TIMEZONE`.
            Defaults to :meth:`datetime.datetime.now()`.
        times: If passed will only consider servings of the specified times. Defaults to all.
        verbose (:obj:`bool`, optional): If :obj:`True`, will contain information on
            allergens and additives. Defaults to :obj:`False`.
        navigation_keyboard: Optional. If :obj:`True`, will append to buttons to navigate through
            the cafeterias as expected by
            :attr:`bot.callbackqueries.next_serving_callback_query`. Defaults to :obj:`True`.

            Note:
                Will *always* append buttons to toggle verbosity, refresh, show the legend and to
                show the online menu.

    Returns:
        The keyword arguments dictionary.

    Raises:
        ValueError: For invalid :attr:`method` input.
    """
    if method not in ALL_METHODS:
        raise ValueError('Invalid method')

    serving_collection = cast(ServingsCollection, context.bot_data[SERVING_COLLECTIONS][cid])
    text, serving = serving_collection.get_next_serving_text(
        from_dtm=from_dtm,
        times=times,
        allow_empty=False,
        html=True,
        verbose=verbose,
        truncate=constants.MAX_MESSAGE_LENGTH,
    )
    return _serving_kwargs(
        serving=serving,
        method=method,
        context=context,
        verbose=verbose,
        text=text,
        cid=cid,
        location=serving_collection.location,
        navigation_keyboard=navigation_keyboard,
    )


@overload
def serving_kwargs(
    serving: Serving,
    method: Literal['send_message', 'edit_message_text'],
    context: CallbackContext,
    verbose: bool = False,
    navigation_keyboard: bool = True,
) -> SendTextDict:
    ...


@overload
def serving_kwargs(
    serving: Serving,
    method: Literal['answer_inline_query'],
    context: CallbackContext,
    verbose: bool = False,
    navigation_keyboard: bool = True,
) -> AnswerInlineDict:
    ...


def serving_kwargs(  # pylint: disable=R0914
    serving: Serving,
    method: Literal['answer_inline_query', 'send_message', 'edit_message_text'],
    context: CallbackContext,
    verbose: bool = False,
    navigation_keyboard: bool = True,
) -> Union[SendTextDict, AnswerInlineDict]:
    """
    Gives the keyword arguments needed to display the passed serving. Can be
    used with :attr:`telegram.Bot.send_message` :attr:`telegram.Bot.edit_message_text` and
    :attr:`telegram.Bot.answer_callback_query`:

    .. code:: python

        bot.send_message(chat_id=chat_id, **serving_kwargs(serving, 'send_message', context, cid))
        bot.edit_message_text(
            chat_id=chat_id,
            message_id=message_id,
            **serving_kwargs(serving, 'edit_message_text', context, cid)
        )
        inline_kwargs = serving_kwargs(serving, 'answer_inline_query', context, cid)
        bot.answer_callback_query(
            InlineQueryResultArticle(
                id=id,
                title=title,
                input_message_content=InputTextMessageContent(inline_kwargs['message_text']),
                reply_markup=inline_kwargs['reply_markup'],
            )
        )

    Args:
        method: The method to produce the kwargs for. Use one of :attr:`SEND_TEXT`,
            :attr:`EDIT_TEXT` and :attr:`ANSWER_INLINE`.
        context: The callback context as provided by the dispatcher.
        verbose (:obj:`bool`, optional): If :obj:`True`, will contain information on
            allergens and additives. Defaults to :obj:`False`.
        navigation_keyboard: Optional. If :obj:`True`, will append to buttons to navigate through
            the cafeterias as expected by
            :attr:`bot.callbackqueries.next_serving_callback_query`. Defaults to :obj:`True`.

            Note:
                Will *always* append buttons to toggle verbosity, refresh, show the legend and to
                show the online menu.

    Returns:
        The keyword arguments dictionary.

    Raises:
        ValueError: For invalid :attr:`method` input.
    """
    if method not in ALL_METHODS:
        raise ValueError('Invalid method')
    return _serving_kwargs(
        serving=serving,
        method=method,
        context=context,
        verbose=verbose,
        navigation_keyboard=navigation_keyboard,
    )


@overload
def _serving_kwargs(  # pylint: disable=R0914
    *,
    method: Literal['answer_inline_query', 'send_message', 'edit_message_text'],
    context: CallbackContext,
    text: str,
    cid: int,
    location: Cafeteria,
    serving: Serving = None,
    verbose: bool = False,
    navigation_keyboard: bool = True,
) -> Union[SendTextDict, AnswerInlineDict]:
    ...


@overload
def _serving_kwargs(  # pylint: disable=R0914
    *,
    method: Literal['answer_inline_query', 'send_message', 'edit_message_text'],
    context: CallbackContext,
    serving: Serving,
    verbose: bool = False,
    navigation_keyboard: bool = True,
) -> Union[SendTextDict, AnswerInlineDict]:
    ...


def _serving_kwargs(  # type: ignore[misc] # pylint: disable=R0914
    *,
    method: Literal['answer_inline_query', 'send_message', 'edit_message_text'],
    context: CallbackContext,
    text: str = None,
    cid: str = None,
    location: Cafeteria = None,
    serving: Serving = None,
    verbose: bool = False,
    navigation_keyboard: bool = True,
) -> Union[SendTextDict, AnswerInlineDict]:
    if method not in ALL_METHODS:
        raise ValueError('Invalid method')

    effective_cid = cast(int, serving.location.id if serving else cid)

    online_menu = (
        location.menu_button  # type: ignore[union-attr]
        if serving is None
        else serving.location.menu_button
    )
    refresh = InlineKeyboardButton(
        'Aktualisieren 🔄', callback_data=REFRESH_DATA.format(effective_cid, verbose)
    )

    if serving:
        toggle_verbosity = InlineKeyboardButton(
            'Weniger Info 🔼' if verbose else 'Mehr Info 🔽',
            callback_data=TOGGLE_VERBOSITY_DATA.format(
                effective_cid, serving.date, serving.time, not verbose
            ),
        )
        legend = InlineKeyboardButton(
            'Legende 📜',
            callback_data=SHOW_LEGEND_DATA.format(effective_cid, serving.date, serving.time),
        )

        if serving.effective_tags().categories:
            buttons = [[toggle_verbosity, legend], [refresh, online_menu]]
        else:
            buttons = [[toggle_verbosity], [refresh, online_menu]]
    else:
        buttons = [[refresh, online_menu]]

    if navigation_keyboard:

        next_id = next_cafeteria(effective_cid)
        prev_id = next_cafeteria(effective_cid, reverse=True)
        next_sc = cast(ServingsCollection, context.bot_data[SERVING_COLLECTIONS][next_id])
        prev_sc = cast(ServingsCollection, context.bot_data[SERVING_COLLECTIONS][prev_id])
        navigation_buttons = [
            [
                InlineKeyboardButton(
                    f'⬅️   {prev_sc.location.short_name}',
                    callback_data=SHOW_NEXT_DATA.format(prev_id, verbose),
                ),
                InlineKeyboardButton(
                    f'{next_sc.location.short_name}   ➡️',
                    callback_data=SHOW_NEXT_DATA.format(next_id, verbose),
                ),
            ],
        ]
        buttons.extend(navigation_buttons)
    keyboard = InlineKeyboardMarkup(buttons)

    if not text:
        effective_text = serving.pprint(  # type: ignore[union-attr]
            verbose=verbose, truncate=constants.MAX_MESSAGE_LENGTH, html=True
        )
    else:
        effective_text = text
    if method in [SEND_TEXT, EDIT_TEXT]:

        return {'text': effective_text, 'reply_markup': keyboard}
    return {'message_text': effective_text, 'reply_markup': keyboard}


Callback = Callable[[Update, CallbackContext], Any]
Func = TypeVar('Func', bound=Callback)


@overload
def send_chat_action(__func: Func) -> Func:
    ...


@overload
def send_chat_action(*, chat_action: str = ChatAction.TYPING) -> Callable[[Func], Func]:
    ...


def send_chat_action(
    __func: Callback = None,
    *,
    chat_action: str = ChatAction.TYPING,
) -> Union[Callable[[Callback], Callback], Callback]:
    """
    Decorator (factory) to send chat action while processing a callback.

    Args:
        chat_action: The chat action to send. Defaults to :attr:`telegram.ChatAction.TYPING`.
    """

    def decorator(func: Callback) -> Callback:
        @wraps(func)
        def wrapper(update: Update, context: CallbackContext) -> Any:
            if update.effective_chat:
                context.bot.send_chat_action(chat_id=update.effective_chat.id, action=chat_action)
            return func(update, context)

        return wrapper

    if __func is not None:
        return decorator(__func)
    return decorator
