#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Objects and methods for the bot functionality."""
from .setup import setup_dispatcher  # type: ignore[attr-defined]

__all__ = ['setup_dispatcher']
