#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Command callbacks."""
from typing import cast, Dict

from telegram import Update, Message
from telegram.ext import CallbackContext

from .utils import next_serving_kwargs, SEND_TEXT, send_chat_action
from .constants import ABBRVS, VERBOSITY_KEY


@send_chat_action
def next_menu_command(update: Update, context: CallbackContext) -> None:
    """
    Returns the next available menu for the selected cafeteria.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    message = cast(Message, update.effective_message)
    command = message.parse_entity(message.entities[0]).split('@')[0].strip('/')

    verbose = cast(Dict, context.user_data).setdefault(VERBOSITY_KEY, False)

    message.reply_text(
        **next_serving_kwargs(  # type: ignore[call-overload]
            SEND_TEXT, context, ABBRVS[command], verbose=verbose
        )
    )
