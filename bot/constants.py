#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Global constants."""
import re
from typing import Dict, Pattern

HOMEPAGE: str = 'https://hirschheissich.gitlab.io/tubs-mensa-bot/'
""":obj:`str`: Homepage of this bot."""
USER_GUIDE: str = f'{HOMEPAGE}usage.html'
""":obj:`str`: User guide for this bot."""
ABBRVS: Dict[str, int] = {
    'm1': 101,
    'm2': 105,
    'cm2': 106,
    '360': 111,
    'b4u': 109,
    'kk': 120,
    'truck': 194,
}
"""dict[:obj:`str`, :obj:`int`]: Maps the abbreviations of the cafeterias to their IDs."""

# Constants for callback_data and friends
SHOW_NEXT_DATA: str = 'show_next {} {}'
""":obj:`str`: Unique starting pattern for :class:`telegram.InlineKeyboardButton` s leading to the
next Cafeteria. Insert the next cafeterias ID as ``SHOW_NEXT_DATA.format(cid, verbose)``"""
SHOW_NEXT_PATTERN: Pattern = re.compile(r'^show_next (\d+) (True|False)$')
"""
:class:`re.Pattern`: Pattern to catch :attr:`SHOW_NEXT_DATA`. The desired cafeterias ID  and the
verbosity level will be available as ``context.match.group(1-2)``.
"""
SHOW_LEGEND_DATA: str = 'legend {} {} {}'
"""
:obj:`str`: Unique starting pattern for :class:`telegram.InlineKeyboardButton` s leading to the
legend of a serving. Insert the data as::

    SHOW_LEGEND_DATA.format(cafeterias_id, iso-date, time)
"""
SHOW_LEGEND_PATTERN: Pattern = re.compile(r'^legend (\d+) ([\d-]+) (\w+)$')
"""
:class:`re.Pattern`: Pattern to catch :attr:`SHOW_LEGEND_DATA`. The desired cafeterias ID, date
and time will be available as ``context.match.group(1-3)``.
"""
TOGGLE_VERBOSITY_DATA: str = 'verb {} {} {} {}'
"""
:obj:`str`: Unique starting pattern for :class:`telegram.InlineKeyboardButton` s toggling the
verbosity of the serving. Insert the data as::

    TOGGLE_VERBOSITY_DATA.format(cafeterias_id, iso-date, time, verbosity)

where ``verbosity`` is a boolean indicating whether the button should lead to verbosity or not.
"""
TOGGLE_VERBOSITY_PATTERN: Pattern = re.compile(r'^verb (\d+) ([\d-]+) (\w+) (True|False)$')
"""
:class:`re.Pattern`: Pattern to catch :attr:`TOGGLE_VERBOSITY_DATA`. The desired cafeterias ID,
date, time and verbosity level will be available as ``context.match.group(1-4)``.
"""
REFRESH_DATA: str = 'refresh {} {}'
""":obj:`str`: Unique starting pattern for :class:`telegram.InlineKeyboardButton` s leading to the
message being refreshed. Insert the next cafeterias ID as ``REFRESH_DATA.format(cid, verbose)``"""
REFRESH_PATTERN: Pattern = re.compile(r'^refresh (\d+) (True|False)$')
"""
:class:`re.Pattern`: Pattern to catch :attr:`REFRESH_DATA`. The desired cafeterias ID and the
verbosity level will be available as ``context.match.group(1-2)``.
"""
NO_MENU_DATA: str = 'no_inline_menu_{}'
""":obj:`str`: Unique starting pattern for ``switch_pm`` start commands resulting from no menu
found in inline mode. Insert the next cafeterias ID as ``NO_MENU_DATA.format(cid)``"""
NO_MENU_PATTERN: Pattern = re.compile(r'^/start no_inline_menu_(\d*)$')
"""
:class:`re.Pattern`: Pattern to catch :attr:`NO_MENU_PATTERN`. The desired cafeterias ID will be
available as ``context.match.group(1)``. May be empty.
"""


# Constants bot/chat/user_data
SERVING_COLLECTIONS: str = 'serving_collections'
"""
:obj:`str`: Key for :attr:`telegram.ext.Dispatcher.bot_data`. Will contain the serving collections
for cafeterias.
"""
ADMIN_KEY: str = 'ADMIN_KEY'
"""
:obj:`str`: Key for :attr:`telegram.ext.Dispatcher.bot_data`. Will contain the admins chat id.
"""
VERBOSITY_KEY: str = 'verbosity_setting'
"""
:obj:`str`: Key for :attr:`telegram.ext.Dispatcher.user_data`. If present, will contain the users
last used verbosity setting.
"""
