#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""CallbackQuery callbacks."""
import datetime as dtm
from typing import cast, Literal, Match, Dict

from telegram import Update, CallbackQuery
from telegram.error import BadRequest
from telegram.ext import CallbackContext

from stwon import ServingsCollection, Serving
from utils import str2bool
from .constants import SERVING_COLLECTIONS, VERBOSITY_KEY
from .utils import next_serving_kwargs, EDIT_TEXT, serving_kwargs


def next_serving_callback_query(update: Update, context: CallbackContext) -> None:
    """
    Updates the message with the next available menu for the selected cafeteria.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    match = cast(Match, context.match)
    cid = int(match.group(1))
    verbose = str2bool(match.group(2))

    callback_query = cast(CallbackQuery, update.callback_query)
    callback_query.answer()
    try:
        callback_query.edit_message_text(
            **next_serving_kwargs(  # type: ignore[call-overload]
                EDIT_TEXT, context, cid, verbose=verbose
            )
        )
    except BadRequest as exc:
        if 'Message is not modified' in str(exc):
            pass
        else:
            raise exc


def show_legend(update: Update, context: CallbackContext) -> None:
    """
    Shows the servings legend as pop up.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    match = cast(Match, context.match)
    cid = int(match.group(1))
    date = dtm.date.fromisoformat(match.group(2))
    time = match.group(3)

    serving_collection = cast(ServingsCollection, context.bot_data[SERVING_COLLECTIONS][cid])
    serving = cast(Serving, serving_collection.servings[date][time])
    legend = serving.effective_tags().pprint_categories(multiline=True, truncate=200)

    cast(CallbackQuery, update.callback_query).answer(show_alert=True, text=legend)


def toggle_verbosity(update: Update, context: CallbackContext) -> None:
    """
    Toggles verbosity of a serving.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    match = cast(Match, context.match)
    cid = int(match.group(1))
    date = dtm.date.fromisoformat(match.group(2))
    time = match.group(3)
    verbose = str2bool(cast(Literal['True', 'False'], match.group(4)))

    # Update chats verbosity settings
    cast(Dict, context.user_data)[VERBOSITY_KEY] = verbose

    serving_collection = cast(ServingsCollection, context.bot_data[SERVING_COLLECTIONS][cid])
    serving = cast(Serving, serving_collection.servings[date][time])

    callback_query = cast(CallbackQuery, update.callback_query)
    callback_query.answer()
    callback_query.edit_message_text(
        **serving_kwargs(  # type: ignore[call-overload]
            method=EDIT_TEXT, context=context, serving=serving, verbose=verbose
        )
    )
