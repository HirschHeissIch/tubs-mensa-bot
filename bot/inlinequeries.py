#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""InlineQuery callbacks."""
import datetime as dtm
from typing import Optional, TypedDict, Literal, List, Dict, Tuple, cast, Match
from fuzzywuzzy import process
from telegram import (
    Update,
    InlineQueryResultArticle,
    InputTextMessageContent,
    InlineQuery,
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    Message,
)
from telegram.ext import CallbackContext
from stwon import OpeningHour, get_cafeteria, Serving, ServingsCollection
from utils import (
    WEEKDAYS_LONG,
    WEEKDAYS_SHORT,
    weekday2date,
    DAY_SHORTCUTS,
    shortcut2date,
)

from .constants import SERVING_COLLECTIONS, ABBRVS, NO_MENU_DATA, VERBOSITY_KEY
from .utils import serving_kwargs


class TimesDict(TypedDict):  # pylint: disable=C0112
    """"""  # hide from docs

    früh: Literal['morning']
    mittag: Literal['noon']
    abend: Literal['evening']


class KwargsDict(TypedDict, total=False):  # pylint: disable=C0112
    """"""  # hide from docs

    date: List[str]
    time: List[str]
    location: List[str]


TIMES: TimesDict = {'früh': 'morning', 'mittag': 'noon', 'abend': 'evening'}
"""
Dict[:obj:`str`, :obj:`str`]: Dictionary with accepted identifiers of serving times as keys and
the API constant equivalents as values.
"""
CAFETERIA_NAMES = {get_cafeteria(cid).name: cid for cid in ABBRVS.values()}
"""
Dict[:obj:`str`, :obj:`int`]: Dictionary with accepted long name identifiers of cafeterias as keys
and the corresponding IDs as values.
"""

_ALL_KEYWORDS = (
    list(TIMES.keys())
    + list(DAY_SHORTCUTS.keys())
    + list(ABBRVS.keys())
    + list(CAFETERIA_NAMES.keys())
    + list(WEEKDAYS_LONG)
    + list(WEEKDAYS_SHORT)
)

DATE_KEYWORDS: List[str] = (
    list(DAY_SHORTCUTS.keys()) + list(WEEKDAYS_SHORT.keys()) + list(WEEKDAYS_LONG.keys())
)
"""
List[:obj:`str`]: Keywords that indicate a date.
"""
TIME_KEYWORDS: List[str] = list(TIMES.keys())
"""
List[:obj:`str`]: Keywords that indicate a serving time.
"""
LOCATION_KEYWORDS: List[str] = list(CAFETERIA_NAMES.keys()) + list(ABBRVS.keys())
"""
List[:obj:`str`]: Keywords that indicate a cafeteria/location.
"""


def parse_query(
    query: str,
) -> Tuple[Optional[List[int]], Optional[List[dtm.date]], Optional[List[str]]]:
    """
    Parses an inline query and tries to extract a cafeterias ID, a date and a serving time from it.
    All of those are optional and maybe have multiple values.

    Args:
        query: The inline query text.

    Returns:
        Tuple[Optional[List[:obj:`int`]], Optional[List[:obj:`dtm`].date], \
            Optional[List[:obj:`str`]]]: Tuple describing cafeteria ID/s, date/s and serving time/s
    """

    def map_type(str_input: str) -> Literal['date', 'time', 'location']:
        if str_input in DATE_KEYWORDS:
            return 'date'
        if str_input in TIME_KEYWORDS:
            return 'time'
        return 'location'

    keywords = query.strip().split()
    kwargs: KwargsDict = {}

    for keyword in keywords:
        closest_match = process.extractOne(keyword, _ALL_KEYWORDS, score_cutoff=50)
        if closest_match[0] is None:
            continue

        kw_type = map_type(closest_match[0])
        if kw_type in kwargs:
            kwargs[kw_type].append(closest_match[0])
        else:
            kwargs[kw_type] = [closest_match[0]]

    cid: List[int] = []
    if 'location' in kwargs:
        for loc in kwargs['location']:
            cid.append(
                CAFETERIA_NAMES.get(loc, None) or ABBRVS.get(loc, None)  # type: ignore[arg-type]
            )

    dates: List[dtm.date] = []
    if 'date' in kwargs:
        for date in kwargs['date']:
            if date in DAY_SHORTCUTS:
                dates.append(shortcut2date(date))  # type: ignore[arg-type]
            else:
                dates.append(weekday2date(date))

    time = kwargs.get('time', None)

    return cid or None, dates or None, time


def build_article(serving: Serving, context: CallbackContext) -> InlineQueryResultArticle:
    """
    Given a serving, builds a corresponding :class:`telegram.InlineQueryResultArticle`.

    Args:
        serving: The serving.
        context: The context as provided by the dispatcher.

    """
    verbose = cast(Dict, context.user_data).setdefault(VERBOSITY_KEY, False)
    inline_kwargs = serving_kwargs(
        serving=serving, method='answer_inline_query', context=context, verbose=verbose
    )
    return InlineQueryResultArticle(
        id=f'{serving.location.id}-{serving.date}-{serving.time}',
        title=f'{serving.location.name}: {serving.pprint_time}',
        description=f'{serving.pprint_date}, {serving.opening_hour.pprint}',
        input_message_content=InputTextMessageContent(inline_kwargs['message_text']),
        reply_markup=inline_kwargs['reply_markup'],
    )


def servings_inline_query(update: Update, context: CallbackContext) -> None:
    """
    Based on the :class:`telegram.InlineQuery`, displays a list of menus for the user to choose
    from.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    inline_query = cast(InlineQuery, update.inline_query)
    query = inline_query.query

    servings_collections = cast(
        Dict[int, ServingsCollection], context.bot_data[SERVING_COLLECTIONS]
    )

    parsed = parse_query(query)
    if not parsed:
        results = [
            build_article(sv, context)
            for cid in servings_collections.keys()
            if (sv := servings_collections[cid].get_next_serving()) is not None and sv.meals
        ]
    else:
        effective_ids = parsed[0] if parsed[0] is not None else list(servings_collections.keys())
        effective_times = parsed[2] if parsed[2] is not None else OpeningHour.ALL_TIMES
        effective_dates = parsed[1]

        # Build results
        results = []
        for cid in effective_ids:
            servings_collection = servings_collections[cid]
            if not effective_dates:
                next_serving = servings_collection.get_next_serving(
                    times=effective_times  # type: ignore[arg-type]
                )
                servings = (
                    [next_serving] if next_serving is not None and next_serving.meals else []
                )
            else:
                servings = [
                    sv  # type: ignore[has-type]
                    for time in effective_times
                    for dt in effective_dates  # pylint: disable=E1133
                    if (sv := servings_collection.servings[dt][time]) is not None  # type: ignore
                    and sv.meals  # type: ignore[has-type]
                ]
            results.extend([build_article(serving, context) for serving in servings])

    # If we didn't find a menu, redirect to online menu
    if not results:
        inline_query.answer(
            switch_pm_text='❌ Kein Menü gefunden.',
            switch_pm_parameter=NO_MENU_DATA.format(
                effective_ids[0] if len(effective_ids) == 1 else ''
            ),
            results=[],
        )

    inline_query.answer(results, cache_time=0, auto_pagination=True)


def answer_deep_linking(update: Update, context: CallbackContext) -> None:
    """
    Answers start messages resulting from no menu found in inline mode.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    match = cast(Match, context.match)
    servings_collections = cast(
        Dict[int, ServingsCollection], context.bot_data[SERVING_COLLECTIONS]
    )
    text_template = (
        'Im Inline-Mode wurde keine Menü{} gefunden. '
        'Bitte sieh ggf. noch einmal im Online-Speiseplan nach.'
    )
    if match.group(1):
        location = servings_collections[int(match.group(1))].location
        text = text_template.format(f' für {location.name}')
        button = location.menu_button
    else:
        button = InlineKeyboardButton(
            'Online-Speiseplan 🍽',
            url='https://www.stw-on.de/braunschweig/essen/mensen-cafeterien/',
        )
        text = text_template.format('')

    cast(Message, update.effective_message).reply_text(
        text=text, reply_markup=InlineKeyboardMarkup.from_button(button)
    )
