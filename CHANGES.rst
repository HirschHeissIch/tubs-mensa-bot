=========
Changelog
=========

Version 2.0.3
=============
*Released 2021-04-09*

**Minor changes:**

* Update handling of ``stwon.Tags.special`` (now handled as list of ``stwon.MealLabel``)

Version 2.0.2
=============
*Released 2021-03-29*

**Minor changes:**

* Fix bug in ``stwon.ServingsCollection.register_announcement``
* Incorporate API update regarding special tags

Version 2.0.1
=============
*Released 2021-01-11*

**New features**

* Remembers verbosity level per message across cafeterias & refresh
* Remembers users last verbosity level setting and applies it when sending new messages

Version 2.0
===========
*Released 2021-01-01*

This version is almost a complete rewrite of the bot. This is due to two important changes:

* The bot now uses the new HTTP API of the Stw ON
* The bot was upgraded to PTB v13.1

As most cafeterias are either closed or have limited opening times, testing was a bit difficult, so be prepared for bugs.

Version 1.1.2
=============
*Released 2020-09-08*

**New features**

* Support for the Food Truck (``/truck``)

Version 1.1.1
=============
*Released 2020-05-08*

**New features**

* Support for the Cafeteria Mensa 2 (``/cm2``)

Version 1.1
=============
*Released 2020-03-18*

**New features**

* Use config file ``bot.ini`` for customizing bot token and admins chat id

**Bug fixes**

* Handle closed cafeterias
* Bug fix for inline mode regarding the 360 Grad
* Correctly search for next menu in ``pprint_next_menu``

Version 1.0.2
=============
*Released 2020-03-17*

**Bug Fixes**

- Strip bot name from bot commands in groups

Version 1.0.1
=============
*Released 2020-03-16*

**Minor Changes**

- Make reading of bot token from file more robust
- Use build in HTML parser instead of ``lxml`` because of installation difficulties
- List ``requests`` in ``requirements.txt``

Version 1.0
===========
*Released 2020-03-16*

**New features**

- Simple bot commands for showing info about the bot
- Bot commands for showing the next available menu
- Inline mode for selecting a specific menu
- Buttons to navigate through the cafeterias
