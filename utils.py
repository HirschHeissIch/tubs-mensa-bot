#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import locale
import sys
import threading
import datetime as dtm
from typing import Dict, Any, Literal, Iterator, TYPE_CHECKING, overload
from contextlib import contextmanager

import pytz

if TYPE_CHECKING:
    from stwon import ServingsCollection

LOCALE_LOCK = threading.Lock()


@contextmanager
def setlocale(name: str):  # type: ignore[no-untyped-def]
    """
    Wrapper to set locale. Use like::

        with setlocale('de_DE.UTF-8'):
            return string.strptime('%B %Y')
    """
    with LOCALE_LOCK:
        saved = locale.setlocale(locale.LC_ALL)
        try:
            yield locale.setlocale(locale.LC_ALL, name)
        finally:
            locale.setlocale(locale.LC_ALL, saved)


def date_range(start_date: dtm.date, end_date: dtm.date) -> Iterator[dtm.date]:
    """
    Date-Equivalent of :meth:`range`.

    Note:
        :attr:`end_date` will *not* be included.

    Args:
        start_date: The start date.
        end_date: The end date.

    """
    for n in range(int((end_date - start_date).days)):
        yield start_date + dtm.timedelta(n)


DAY_SHORTCUTS = {'heute': 0, 'morgen': 1, 'übermorgen': 2}
"""
Dict[:obj:`str`, :obj:`int`]: Parsable shortcuts to describe days relative to today as keys and
the corresponding days-offset relative to today as values.
"""
with setlocale('de_DE.UTF-8'):
    WEEKDAYS_LONG = {
        date.strftime('%A').lower(): date.weekday()
        for date in date_range(dtm.date(2020, 12, 28), dtm.date(2021, 1, 4))
    }
    """
    Dict[:obj:`str`, :obj:`str`]: Dictionary with long name identifiers weekdays as keys
    and the corresponding 0-based integers as values.
    """
    WEEKDAYS_SHORT = {
        date.strftime('%a').lower(): date.weekday()
        for date in date_range(dtm.date(2020, 12, 28), dtm.date(2021, 1, 4))
    }
    """
    Dict[:obj:`str`, :obj:`str`]: Dictionary with short name identifiers weekdays as keys
    and the corresponding 0-based integers as values.
    """
_WEEKDAYS_LOWER = {wd.lower(): day for wd, day in WEEKDAYS_LONG.items()}
for wd, day in WEEKDAYS_SHORT.items():
    _WEEKDAYS_LOWER[wd.lower()] = day


def next_weekday(day: int) -> dtm.date:
    """
    Gives the date of the next weekday specified by ``day``. Will never be today.

    Args:
        day: 0-based Specification of the weekday.

    """
    today = dtm.date.today()
    if today.weekday() == day:
        return today + dtm.timedelta(days=7)
    return today + dtm.timedelta(days=(day - today.weekday()) % 7)


def weekday2date(day: str) -> dtm.date:
    """
    Given an (abbreviation of a) weekday in the german format, returns a :obj:`datetime.date` of
    the *next* date with the corresponding weekday. Will never be today, even if the weekday fits.

    Args:
        day (:obj:`str`): The weekday abbreviation. Leading or trailing whitespaces and periods
            will be stripped. Case insensitive.

    Raises:
        ValueError: For invalid input.

    """
    key = day.strip().lower()
    if key not in _WEEKDAYS_LOWER:
        raise ValueError('No valid day.')
    return next_weekday(_WEEKDAYS_LOWER[key])


def shortcut2date(shortcut: Literal['heute', 'morgen', 'übermorgen']) -> dtm.date:
    """
    Given one of :attr:`DAY_SHORTCUTS` gives the corresponding :obj:`datetime.date` relative to
    :meth:`datetime.date.today()`

    Args:
        shortcut: The shortcut. Leading or trailing whitespaces and periods
            will be stripped. Case insensitive.

    Raises:
        ValueError: For invalid input.

    """
    key = shortcut.strip().lower()
    if key not in DAY_SHORTCUTS:
        raise ValueError('No valid shortcut.')
    return dtm.date.today() + dtm.timedelta(days=DAY_SHORTCUTS[key])


TIMEZONE: pytz.BaseTzInfo = pytz.timezone('Europe/Berlin')
"""TIMEZONE :class:`pytz.BaseTzInfo`: Timezone object for Europe/Berlin"""


def parse_iso_time(iso_time: str) -> dtm.time:
    """
    Parses an ISO time (e.g. ``11:30``) and returns a :class:`datetime.time` object localized
    as :attr:`TIMEZONE`.

    Args:
        iso_time: The ISO date.

    Returns:
        The localized date object.

    """
    return TIMEZONE.localize(
            dtm.datetime.combine(dtm.date.today(), dtm.time.fromisoformat(iso_time))
        ).timetz()


JSONDict = Dict[str, Any]
"""Dictionary containing response from or data to send to the API."""
SCDict = Dict[int, 'ServingsCollection']
"""Dictionary for every cafeterias ID the corresponding :class:`stwon.ServingCollection`."""
TimesLiteral = Literal['morning', 'noon', 'evening']
"""Literals allowed for the :attr:`time` attribute of the various stwon types."""


@overload
def str2bool(str_input: Literal['True']) -> Literal[True]: ...


@overload
def str2bool(str_input: Literal['False']) -> Literal[False]: ...


def str2bool(str_input: Literal['True', 'False']) -> bool:
    """
    Shortcut for converting ``str(boolean_input)`` back to a boolean.

    Args:
        str_input: The input.
    """
    return str_input == 'True'


def remove_prefix(text: str, prefix: str) -> str:
    """
    Backwards compatible wrapper for :meth:`str.removeprefix`.

    Args:
        text: The text to remove the prefix from.
        prefix: The prefix to remove.

    """
    if sys.version_info >= (3, 9):
        return str.removeprefix(text, prefix)
    if text.startswith(prefix):
        return text[len(prefix):]
    return text
