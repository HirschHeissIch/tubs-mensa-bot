#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module contains the StwOnObject class."""
import json
import warnings
from typing import List, Type, Optional, TypeVar, Tuple, Any

from utils import JSONDict

TO = TypeVar('TO', bound='StwOnObject', covariant=True)


class StwOnObject:
    """
    Base class for most stwon objects.
    """

    _id_attrs: Tuple[Any, ...] = ()

    @staticmethod
    def parse_data(data: Optional[JSONDict]) -> Optional[JSONDict]:
        """
        Pre-process incoming JSON data. Should be called by :meth:`de_json`.

        Args:
            data: The JSON data.

        Returns:
            Pre-precessed JSON data.

        """
        if not data:
            return None
        return data.copy()

    @classmethod
    def de_json(cls: Type[TO], data: Optional[JSONDict]) -> Optional[TO]:
        """
        Constructs an instance from JSON data. Should call :meth:`parse_data`.

        Args:
            data: The JSON data.

        Returns:
            The instance.

        """
        data = cls.parse_data(data)

        if not data:
            return None

        return cls(**data)  # type: ignore[call-arg]

    @classmethod
    def de_list(cls: Type[TO], data: Optional[List[JSONDict]]) -> List[Optional[TO]]:
        """
        Constructs a list of instances from JSON data.

        Args:
            data: The JSON data.

        Returns:
            The list of instances.

        """
        if not data:
            return []

        return [cls.de_json(d) for d in data]

    def to_json(self) -> str:
        """
        Returns:
            JSON representation of the object.
        """
        return json.dumps(self.to_dict())

    def to_dict(self) -> JSONDict:
        """
        Returns:
            Dict representation of the object.
        """
        data = dict()

        for key in iter(self.__dict__):
            if key.startswith('_'):
                continue

            value = self.__dict__[key]
            if value is not None:
                if isinstance(value, StwOnObject):
                    data[key] = value.to_dict()
                else:
                    data[key] = value

        return data

    def __eq__(self, other: object) -> bool:
        if isinstance(other, self.__class__):
            if self._id_attrs == ():
                warnings.warn(
                    f"Objects of type {self.__class__.__name__} can not be meaningfully tested for"
                    " equivalence."
                )
            if other._id_attrs == ():
                warnings.warn(
                    f"Objects of type {other.__class__.__name__} can not be meaningfully tested"
                    " for equivalence."
                )
            return self._id_attrs == other._id_attrs
        return super().__eq__(other)  # pylint: disable=no-member

    def __hash__(self) -> int:
        if self._id_attrs:
            return hash((self.__class__, self._id_attrs))  # pylint: disable=no-member
        return super().__hash__()
