#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module contains the Serving class."""
import datetime as dtm
from functools import cached_property

from typing import List, ClassVar

from utils import setlocale, TimesLiteral
from .stwonobject import StwOnObject
from .announcement import Announcement
from .tags import Tags
from .meal import Meal
from .openinghours import OpeningHour
from .cafeteria import Cafeteria


class Serving(StwOnObject):
    """
    Convenience class that collects all meals available during a specific serving on a specific
    date.

    Objects of this class are comparable in terms of equality. Two objects of this class are
    considered equal, if their :attr:`date` and :attr:`time` is equal.

    Args:
        date (:obj:`datetime.date`): The date of this serving.
        opening_hour (:class:`stwon.OpeningHour`): Details about the hours of this serving.
        location (:class:`stwon.Cafeteria`): Details about the cafeteria this serving is available
            at.

    Attributes:
        date (:obj:`datetime.date`): The date of this serving.
        opening_hour (:class:`stwon.OpeningHour`): Details about the hours of this serving.
        location (:class:`stwon.Cafeteria`): Details about the cafeteria this serving is available
            at.
        meals (List[:class:`stwon.Meal`]): List of meals available.
        announcements (List[:class:`stwon.Announcement`]): Announcements relevant for this serving.
    """

    def __init__(
        self,
        date: dtm.date,
        opening_hour: OpeningHour,
        location: Cafeteria,
    ) -> None:
        self.date = date
        self.opening_hour = opening_hour
        self.location = location
        self.meals: List[Meal] = []
        self.announcements: List[Announcement] = []

        self._id_attrs = (self.date, self.time)

    @property
    def time(self) -> TimesLiteral:
        """
        Returns:
            :attr:`self.opening_hour.time`
        """
        return self.opening_hour.time

    @cached_property
    def pprint_time(self) -> str:
        """
        Gives a human readable, german representation of :attr:`time`.

        Returns:
            :obj:`str`: The representation.
        """
        return {'morning': 'Früh', 'noon': 'Mittag', 'evening': 'Abend'}[self.time]

    @cached_property
    def pprint_date(self) -> str:
        """
        Gives a human readable, german representation of :attr:`date`.

        Returns:
            :obj:`str`: The representation.
        """
        with setlocale('de_DE.UTF-8'):
            return self.date.strftime('%A, %d. %B %Y')

    @property
    def end_time(self) -> dtm.time:
        """
        Returns:
            :attr:`self.opening_hour.end_time`
        """
        return self.opening_hour.end_time

    @property
    def start_time(self) -> dtm.time:
        """
        Returns:
            :attr:`self.opening_hour.start_time`
        """
        return self.opening_hour.start_time

    def register_meal(self, meal: Meal) -> bool:
        """
        Checks if the meal is available during this serving and adds it to :attr:`meals` if
        not already registered.

        Args:
            meal: The meal.

        Returns:
            Whether the meal was added or not. Will also return :obj:`True`, if the meal was
            already registered.

        Raises:
            ValueError: If the meal is not meant for this servings cafeteria.

        """
        if meal.location.id != self.location.id:
            raise ValueError('This meal is not served in this servings cafeteria.')

        if meal.date == self.date and meal.time == self.time:
            if meal in self.meals:
                return True
            self.meals.append(meal)
            return True
        return False

    def register_announcement(self, announcement: Announcement) -> bool:
        """
        Checks if the announcement is relevant for this serving and adds it to :attr:`meals` if
        not already registered.

        Args:
            announcement: The announcement.

        Returns:
            Whether the announcement was added or not. Will also return :obj:`True`, if the
            announcement was already registered.

        """
        if announcement.covers(self.date, self.time):
            if announcement in self.announcements:
                return True
            self.announcements.append(announcement)
            return True
        return False

    def register_meals(self, meals: List[Meal]) -> None:
        """
        Shortcut for::

            for meal in meals:
                serving.register_meal(meal)

        Args:
            meals: The meals.

        """
        for meal in meals:
            self.register_meal(meal)

    def register_announcements(self, announcements: List[Announcement]) -> None:
        """
        Shortcut for::

            for announcement in announcements:
                serving.register_announcement(announcement)

        Args:
            announcements: The announcements.

        """
        for announcement in announcements:
            self.register_announcement(announcement)

    @classmethod
    def build(cls, meal: Meal) -> 'Serving':
        """
        Given a meal creates a new serving such that the meal can be registered to it. Also
        actually registers the meal.

        Args:
            meal: The meal.

        Returns:
            The new serving.

        """
        o_h = meal.location.get_opening_hour(meal.time)
        if not o_h:
            raise RuntimeError('Could not find a suitable OpeningHour slot in the meals location.')

        serving = Serving(meal.date, o_h, location=meal.location)
        serving.register_meal(meal)
        return serving

    def effective_tags(self) -> Tags:
        """
        Gathers all categories, allergens, additives and special tags that appear across the meals
        in :attr:`meals`.

        Returns:
            The gathered labels.

        """
        categories = set()
        allergens = set()
        additives = set()
        special = set()
        for meal in self.meals:
            categories |= set(meal.tags.categories)
            allergens |= set(meal.tags.allergens)
            additives |= set(meal.tags.additives)
            special |= set(meal.tags.special)

        sorted_categories = sorted(categories, key=lambda cat: cat.name.lower())
        sorted_allergens = sorted(allergens, key=lambda al: al.name.lower())
        sorted_additives = sorted(additives, key=lambda add: add.name.lower())
        sorted_special = sorted(special, key=lambda spec: spec.name.lower())

        return Tags(sorted_categories, sorted_allergens, sorted_additives, sorted_special)

    def pprint(  # pylint: disable=R0912,R0914
        self, html: bool = False, verbose: bool = False, truncate: int = 0
    ) -> str:
        """
        Gives a human readable representation of the serving.

        Args:
            html (:obj:`bool`, optional): If :obj:`True`, will contain HTML code as expected by
                :attr:`telegram.Bot.send_message`. Defaults to :obj:`False`.
            verbose (:obj:`bool`, optional): If :obj:`True`, will contain information on
                allergens and additives. Defaults to :obj:`False`.
            truncate (:obj:`int`, optional): Maximum length of the result (disregarding HTML tags).
                 A note will be appended, if the result needs truncation. Truncation happens
                 between meals. Pass ``0`` to not truncate. Defaults to ``0``.

        """
        name = self.location.get_header(
            html=html, closed=any(an.closed for an in self.announcements)
        )
        heading = f'{name}\n{self.pprint_date}, {self.opening_hour.pprint}{self._SEP}'

        if self.announcements:
            if len(self.announcements) > 1:
                announcements = ' • '.join(an.text for an in self.announcements)
            else:
                announcements = self.announcements[0].text
            an_text = f'⚠️ {announcements}{self._SEP}'
        else:
            an_text = ''

        if self.meals:
            meals = self._SEP.join(m.pprint(html=html, verbose=verbose) for m in self.meals)
        else:
            meals = self.NO_MENU_FOUND_NOTE

        result = f'{heading}{an_text}{meals}'
        if not truncate:
            return result

        # Message length is determined *after* entities parsing
        html_less_result = self.pprint(verbose=verbose) if html else result
        if len(html_less_result) > truncate:
            note = f'<i>{self._TRUNCATION_NOTE}</i>' if html else self._TRUNCATION_NOTE
            count = 0

            while len(f'{html_less_result}{self._SEP}{self._TRUNCATION_NOTE}') > truncate:
                count += 1
                temp, _, _ = html_less_result.rpartition(self._SEP)
                html_less_result = temp

            if html:
                result = (
                    self._SEP.join(result.split(self._SEP)[: -count - 1]) + f'{self._SEP}{note}'
                )
            else:
                result = f'{html_less_result}{self._SEP}{note}'

        return result

    _TRUNCATION_NOTE: ClassVar[str] = '[ ... Menü zu lang. Bitte ggf. im Online-Menü nachsehen. ]'
    _SEP: ClassVar[str] = '\n\n'
    NO_MENU_FOUND_NOTE: ClassVar[str] = (
        'Es konnte leider kein Menü gefunden werden. Bitte sieh ggf. noch einmal im '
        'Online-Speiseplan nach.'
    )
    """
    :obj:`str`: Note indicating that no meals were found for this serving.
    """
