#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module contains the Address class."""
from functools import lru_cache

from stwon import StwOnObject
from utils import JSONDict


class Address(StwOnObject):
    """
    Representation of an address of a cafeteria of the Studentenwerk Ostniedersachsen.

    Args:
        line1 (:obj:`str`): First address line.
        line2 (:obj:`str`, optional): Second address line.
        street (:obj:`str`, optional): Street and house number.
        zip (:obj:`int`): Zip code.
        city (:obj:`str`): City.

    Attributes:
        line1 (:obj:`str`): First address line.
        line2 (:obj:`str`): Optional. Second address line.
        street (:obj:`str`): Optional. Street and house number.
        zip (:obj:`int`): Zip code.
        city (:obj:`str`): City.

    """

    def __init__(
        self,
        line1: str,
        zip: int,  # pylint: disable=W0622
        city: str,
        line2: str = None,
        street: str = None,
        **_kwargs: JSONDict,
    ) -> None:
        self.line1 = line1
        self.line2 = line2
        self.street = street
        self.zip = int(zip)
        self.city = city

    @lru_cache(typed=True)
    def pprint(self, indent: int = 0, multiline: bool = False) -> str:
        """
        Gives a human readable representation of the address.

        Args:
            indent (:obj:`int`, optional): Indention to add to the output. Defaults to ``0``.
            multiline (:obj:`bool`, optional): Whether to give a multiline representation. Defaults
                to :obj:`False`.

        Returns:
            str
        """
        idt = indent * ' '
        nl_idt = '\n' + idt
        if multiline:
            return (
                f'{idt}{self.line1}{nl_idt + self.line2 if self.line2 else ""}'
                f'{nl_idt + self.street if self.street else ""}{nl_idt}{self.zip} {self.city}'
            )
        return (
            f'{idt}{self.line1}{", " + self.line2 if self.line2 else ""}'
            f'{", " + self.street if self.street else ""}, {self.zip} {self.city}'
        )
