#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module contains the MealLabel class."""
from functools import cached_property
from typing import Dict, ClassVar

from utils import JSONDict, remove_prefix
from .stwonobject import StwOnObject


class MealLabel(StwOnObject):
    """
    Representation of a meal label.

    Objects of this class are comparable in terms of equality. Two objects of this class are
    considered equal, if their :attr:`id` is equal.

    Args:
        id (:obj:`str`): The ID.
        name (:obj:`str`): The description.

    Attributes:
        id (:obj:`str`): The ID.
        name (:obj:`str`): The description.
    """

    def __init__(
        self,
        id: str,  # pylint: disable=W0622
        name: str,
        **_kwargs: JSONDict,
    ) -> None:
        self.id = id  # pylint: disable=C0103
        self.name = name

        self._id_attrs = (self.id,)

    @cached_property
    def pprint(self) -> str:
        """
        Gives an emoji or short representation for the label if available. Otherwise defaults to
        :attr:`name`.

        Returns:
            :obj:`str`: The representation.
        """
        return remove_prefix(self.EMOJIS.get(self.id, self.name), 'mit ')

    AT: ClassVar[str] = 'AT'
    """:obj:`str`: Animal welfare"""
    EN: ClassVar[str] = 'EN'
    """:obj:`str`: Allergen nut (peanut)"""
    EI: ClassVar[str] = 'EI'
    """:obj:`str`: Allergen egg"""
    FI: ClassVar[str] = 'FI'
    """:obj:`str`: Allergen fish"""
    FISH: ClassVar[str] = 'FISH'
    """:obj:`str`: Fish"""
    GEFL: ClassVar[str] = 'GEFL'
    """:obj:`str`: Poultry"""
    GL1: ClassVar[str] = 'GL1'
    """:obj:`str`: Allergen gluten (wheat)"""
    GL2: ClassVar[str] = 'GL2'
    """:obj:`str`: Allergen gluten (rye)"""
    GL3: ClassVar[str] = 'GL3'
    """:obj:`str`: Allergen gluten (barley)"""
    GL4: ClassVar[str] = 'GL4'
    """:obj:`str`: Allergen gluten (oats)"""
    KLMA: ClassVar[str] = 'KLMA'
    """:obj:`str`: Climate-conscious"""
    LAMM: ClassVar[str] = 'LAMM'
    """:obj:`str`: Lamb"""
    ML: ClassVar[str] = 'ML'
    """:obj:`str`: Allergen milk"""
    MV: ClassVar[str] = 'MV'
    """:obj:`str`: "Mensa Vital"""
    NEU: ClassVar[str] = 'NEU'
    """:obj:`str`: New"""
    NU1: ClassVar[str] = 'NU1'
    """:obj:`str`: Allergen nut (almond)"""
    NU2: ClassVar[str] = 'NU2'
    """:obj:`str`: Allergen nut (hazelnut)"""
    NU3: ClassVar[str] = 'NU3'
    """:obj:`str`: Allergen nut (walnut)"""
    NU4: ClassVar[str] = 'NU4'
    """:obj:`str`: Allergen nut (Cashew nuts)"""
    RIND: ClassVar[str] = 'RIND'
    """:obj:`str`:Beef """
    SCHW: ClassVar[str] = 'SCHW'
    """:obj:`str`: Pork"""
    SE: ClassVar[str] = 'SE'
    """:obj:`str`: Allergen sesame seeds"""
    SF: ClassVar[str] = 'SF'
    """:obj:`str`: Allergen mustard"""
    SL: ClassVar[str] = 'SL'
    """:obj:`str`: Allergen celery"""
    SO: ClassVar[str] = 'SO'
    """:obj:`str`: Allergen soy"""
    SW: ClassVar[str] = 'SW'
    """:obj:`str`: Allergen sulfur oxides"""
    UNKWN: ClassVar[str] = 'UNKWN'
    """:obj:`str`: Unknown Label"""
    VEGA: ClassVar[str] = 'VEGA'
    """:obj:`str`: Vegan"""
    VEGT: ClassVar[str] = 'VEGT'
    """:obj:`str`: Vegetarian"""
    WILD: ClassVar[str] = 'WILD'

    EMOJIS: ClassVar[Dict[str, str]] = {
        AT: '💚',
        EI: 'Ei',
        EN: 'Erdnüsse',
        FI: 'Fisch',
        FISH: '🐟',
        GEFL: '🐔',
        GL1: 'Gluten (Weizen)',
        GL2: 'Gluten (Roggen)',
        GL3: 'Gluten (Gerste)',
        GL4: 'Gluten (Hafer)',
        KLMA: '🌳',
        LAMM: '🐑',
        MV: '💃',
        ML: 'Laktose',
        NU1: 'Mandeln',
        NU2: 'Haselnüsse',
        NU3: 'Walnüsse',
        NU4: 'Kaschunüsse',
        NEU: '🌟',
        RIND: '🐮',
        SCHW: '🐷',
        SE: 'Sesamsamen',
        SF: 'Senf',
        SL: 'Sellerie',
        SO: 'Soja(bohnen)',
        SW: 'Schwefeloxide',
        UNKWN: '⁉️',
        VEGA: '🌱',
        VEGT: '🥕',
        WILD: '🐗',
    }
    """
    :obj:`dict`: A dictionary mapping each label to a fitting emoji alias or short description.
    """
