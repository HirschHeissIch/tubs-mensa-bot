#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module contains the Lane class."""
from utils import JSONDict
from .stwonobject import StwOnObject


class Lane(StwOnObject):
    """
    Representation of a lane.

    Objects of this class are comparable in terms of equality. Two objects of this class are
    considered equal, if their :attr:`id` is equal.

    Args:
        id (:obj:`str`): The ID.
        name (:obj:`str`): The description.

    Attributes:
        id (:obj:`str`): The ID.
        name (:obj:`str`): The description.
    """

    def __init__(
        self,
        id: str,  # pylint: disable=W0622
        name: str,
        **_kwargs: JSONDict,
    ) -> None:
        self.id = id  # pylint: disable=C0103
        self.name = name

        self._id_attrs = (self.id,)
