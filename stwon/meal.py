#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module contains the Meal class."""
from functools import lru_cache
from typing import Optional
import datetime as dtm

from utils import JSONDict, TimesLiteral

from .stwonobject import StwOnObject
from .cafeteria import Cafeteria
from .price import Price
from .tags import Tags
from .lane import Lane


class Meal(StwOnObject):
    """
    Representation of a meal.

    Objects of this class are comparable in terms of equality. Two objects of this class are
    considered equal, if their :attr:`id` is equal.

    Args:
        id (:obj:`int`): The ID of the meal.
        date (:obj:`datetime.date`): The date this meal is available on.
        name (:obj:`str`): The description of the meal.
        price (:class:`stwon.Price`): The price of the meal.
        location (:class:`stwon.Cafeteria`): The caferetia the meal is available in.
        time (:obj:`str`): The serving during which the meal is available.
        lane (:class:`stwon.Lane`): The lane this meal is served at.
        tags (:class:`stwon.Tags`): Information on meal category, allergens and additives


    Attributes:
        id (:obj:`int`): The ID of the meal.
        date (:obj:`datetime.date`): The date this meal is available on.
        name (:obj:`str`): The description of the meal.
        price (:class:`stwon.Price`): The price of the meal.
        location (:class:`stwon.Cafeteria`): The caferetia the meal is available in.
        time (:obj:`str`): The serving during which the meal is available.
        lane (:class:`stwon.Lane`): The lane this meal is served at.
        tags (:class:`stwon.Tags`): Information on meal category, allergens and additives
    """

    DESSERT: str = 'Dessert'
    """:obj:`str`: Type dessert"""
    SIDE: str = 'Beilage'
    """:obj:`str`: Type side dish"""
    VEGETABLES: str = 'Gemüse'
    """:obj:`str`: Type vegetables"""
    GEMUESEGARTEN: str = 'Gemüsegarten'
    """:obj:`str`: Type Gemüsegarten"""

    def __init__(
        self,
        id: int,  # pylint: disable=W0622
        date: dtm.date,
        name: str,
        price: Price,
        location: 'Cafeteria',
        time: TimesLiteral,
        lane: Lane,
        tags: Tags,
        **_kwargs: JSONDict,
    ) -> None:
        self.id = id  # pylint: disable=C0103
        self.date = date
        self.name = name
        self.price = price
        self.location = location
        self.time = time
        self.lane = lane
        self.tags = tags

        self._id_attrs = (self.id,)

    @classmethod
    def de_json(cls, data: Optional[JSONDict]) -> Optional['Meal']:
        data = cls.parse_data(data)

        if not data:
            return None

        data['date'] = dtm.date.fromisoformat(data['date'])
        data['price'] = Price.de_json(data.get('price'))
        data['location'] = Cafeteria.de_json(data.get('location'))
        data['lane'] = Lane.de_json(data.get('lane'))
        data['tags'] = Tags.de_json(data.get('tags'))

        return cls(**data)

    def to_dict(self) -> JSONDict:
        data = super().to_dict()

        data['date'] = self.date.isoformat()
        data['price'] = self.price.to_dict()
        data['location'] = self.location.to_dict()
        data['lane'] = self.lane.to_dict()
        data['tags'] = self.tags.to_dict()

        return data

    @lru_cache(typed=True)
    def pprint(self, html: bool = False, verbose: bool = False) -> str:
        """
        Gives a human readable representation of the meal. Does *not* include :attr:`date`,
        :attr:`time` or :attr:`location`. Intended to be used in an overview of a serving times
        meals.

        Args:
            html (:obj:`bool`, optional): If :obj:`True`, will contain HTML code as expected by
                :attr:`telegram.Bot.send_message`. Defaults to :obj:`False`.
            verbose  (:obj:`bool`, optional): If :obj:`True`, will contain information on
                allergens and additives. Defaults to :obj:`False`.

        """
        desc = f'<b>{self.name}</b>' if html else self.name
        meal = f'{self.lane.name}\n{desc}\n{self.price.pprint}'

        if self.tags.categories:
            meal = f'{meal} | {" ".join(c.pprint for c in self.tags.categories)}'

        if verbose:
            special_tags = ', '.join(s.pprint for s in self.tags.special)
            allergens = ', '.join(a.pprint for a in self.tags.allergens)
            additives = ', '.join(a.pprint for a in self.tags.additives)
            if special_tags:
                meal = f'{meal}\n<i>{special_tags}</i>' if html else f'{meal}\n{special_tags}'
            if allergens:
                if html:
                    meal = f'{meal}\n<i>Allergene:</i> {allergens}'
                else:
                    meal = f'{meal}\nAllergene: {allergens}'
            if additives:
                if html:
                    meal = f'{meal}\n<i>Zusätze:</i> {additives}'
                else:
                    meal = f'{meal}\nZusätze: {additives}'

        return meal
