#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module contains the Serving class."""
import datetime as dtm

from typing import List, ClassVar, TypedDict, Optional, Dict, Tuple

from utils import TimesLiteral, date_range, TIMEZONE
from .serving import Serving
from .openinghours import OpeningHour
from .cafeteria import Cafeteria
from .meal import Meal
from .stwonobject import StwOnObject
from .announcement import Announcement


class DaysServings(TypedDict):  # pylint: disable=C0115
    morning: Optional[Serving]
    noon: Optional[Serving]
    evening: Optional[Serving]


class InnerDict(Dict[TimesLiteral, Optional[Serving]]):  # pylint: disable=C0115,R0903
    def __init__(self, location: Cafeteria, date: dtm.date):
        super().__init__()
        self.location = location
        self.date = date

    def __missing__(self, key: TimesLiteral) -> Optional[Serving]:
        o_h = self.location.get_opening_hour(key)
        if not o_h:
            return None
        self[key] = serving = Serving(self.date, o_h, self.location)  # pylint: disable=E1137
        return serving


class OuterDict(Dict[dtm.date, InnerDict]):  # pylint: disable=C0115,R0903
    def __init__(self, location: Cafeteria):
        super().__init__()
        self.location = location

    def __missing__(self, key: dtm.date) -> InnerDict:
        self[key] = inner_dict = InnerDict(  # pylint: disable=E1137
            location=self.location, date=key
        )
        return inner_dict


class ServingsCollection(StwOnObject):
    """
    Convenience class that collects all servings for a specific cafeteria.

    Objects of this class are comparable in terms of equality. Two objects of this class are
    considered equal, if their :attr:`id`.

    Args:
        location (:class:`stwon.Cafeteria`): Details about the cafeteria this serving is available
            at.

    Attributes:
        location (:class:`stwon.Cafeteria`): Details about the cafeteria this serving is available
            at.
        servings (DefaultDict[:obj:`datetime.date`, DefaultDict[:obj:`str`, \
            Optional[:class:`stwon.Serving`]]): The servings organized by date and time. Thanks to
            the default dict structure, you don't have to check for the keys to exist, though not
            every date may have servings for all times.
    """

    MORNING: ClassVar[str] = 'morning'
    """:obj:`str`: Constant for morning servings."""
    NOON: ClassVar[str] = 'noon'
    """:obj:`str`: Constant for noon servings."""
    EVENING: ClassVar[str] = 'evening'
    """:obj:`str`: Constant for evening servings."""

    def __init__(
        self,
        location: Cafeteria,
    ) -> None:
        self.location = location
        self.servings = OuterDict(location)

        self._id_attrs = (self.id,)

    @property
    def id(self) -> int:  # pylint: disable=C0103
        """
        Shortcut for :attr:`self.location.time`
        """
        return self.location.id

    def register_meal(self, meal: Meal) -> Serving:
        """
        Registers the given meal to the correct serving. Will create a new serving, if needed.

        Args:
            meal: The meal.

        Returns:
            The serving the meal was assigned to.

        Raises:
            ValueError: If the meal is not meant for this collections cafeteria.
            RuntimeError: If there is no suitable opening hour for the meal.

        """
        if meal.location.id != self.id:
            raise ValueError('This meal is not served in this servings collections cafeteria.')

        serving = self.servings[meal.date][meal.time]  # pylint: disable=E1136
        if serving is None:
            raise RuntimeError('Could not find a suitable OpeningHour slot in the meals location.')
        assert serving.register_meal(meal)
        return serving

    def register_meals(self, meals: List[Meal]) -> None:
        """
        Shortcut for::

            for meal in meals:
                self.register_meal(meal)

        Args:
            meals: The meals

        """
        for meal in meals:
            self.register_meal(meal)

    def register_announcement(self, announcement: Announcement) -> List['Serving']:
        """
        Register the given announcement to *all* suitable servings of this collection and creates
        them if needed.

        Args:
            announcement: The announcement.

        Returns:
            The servings the announcement was registered to.

        """
        servings = []

        for date in date_range(
            announcement.start_date, announcement.end_date + dtm.timedelta(days=1)
        ):
            for time in announcement.times:
                serving = self.servings[date][time]  # type: ignore[index]  # pylint: disable=E1136
                if serving:
                    assert serving.register_announcement(announcement)
                    servings.append(serving)

        return servings

    def register_announcements(self, announcements: List[Announcement]) -> None:
        """
        Shortcut for::

            for announcement in announcements:
                self.register_announcement(announcement)

        Args:
            announcements: The announcements

        """
        for announcement in announcements:
            self.register_announcement(announcement)

    def get_next_serving(
        self,
        from_dtm: dtm.datetime = None,
        times: List[TimesLiteral] = None,
        allow_empty: bool = False,
    ) -> Optional[Serving]:
        """
        Gives the next available serving, if there is one.

        Args:
            from_dtm: If passed, will only consider servings available at or after that time.
                Naive objects will be assumed to be in :attr:`utils.TIMEZONE`.
                Defaults to :meth:`datetime.datetime.now()`.
            times: If passed will only consider servings of the specified times. Defaults to all.
            allow_empty: If :obj:`True` will return servings without meals. Defaults to
                :obj:`False`.

        Returns:
            The next serving, if any.

        """
        if from_dtm is None:
            effective_dtm = dtm.datetime.now(TIMEZONE)
        else:
            if from_dtm.tzinfo is None:
                effective_dtm = TIMEZONE.localize(from_dtm)
            else:
                effective_dtm = from_dtm
        e_date = effective_dtm.date()

        effective_times = times or OpeningHour.ALL_TIMES

        for date, servings in self.servings.items():  # pylint: disable=E1101
            if date < e_date:
                continue
            for time in effective_times:
                serving = servings[time]  # type: ignore[index]
                if not serving:
                    continue
                if not allow_empty and not serving.meals:
                    continue
                if (
                    date == e_date and serving.opening_hour.covers(effective_dtm, strict=False)
                ) or date > e_date:
                    return serving

        return None

    def get_next_serving_text(
        self,
        from_dtm: dtm.datetime = None,
        times: List[TimesLiteral] = None,
        allow_empty: bool = False,
        html: bool = False,
        verbose: bool = False,
        truncate: int = 0,
    ) -> Tuple[str, Optional[Serving]]:
        """
        Gives the text next available serving, if there is one, or a note that there is none.

        Args:
            serving: Pass if you already have the next serving. Otherwise :meth:`get_next_serving`
                will be called.
            from_dtm: If passed, will only consider servings available at or after that time.
                Naive objects will be assumed to be in :attr:`utils.TIMEZONE`.
                Defaults to :meth:`datetime.datetime.now()`.
            times: If passed will only consider servings of the specified times. Defaults to all.
            allow_empty: If :obj:`True` will return servings without meals. Defaults to
                :obj:`False`.
            html (:obj:`bool`, optional): If :obj:`True`, will contain HTML code as expected by
                :attr:`telegram.Bot.send_message`. Defaults to :obj:`False`.
            verbose (:obj:`bool`, optional): If :obj:`True`, will contain information on
                allergens and additives. Defaults to :obj:`False`.
            truncate (:obj:`int`, optional): Maximum length of the result (disregarding HTML tags).
                 A note will be appended, if the result needs truncation. Truncation happens
                 between meals. Pass ``0`` to not truncate. Defaults to ``0``.

        Returns:
            The text and the next serving/:obj:`None`.

        """
        if serving := self.get_next_serving(
            from_dtm=from_dtm, times=times, allow_empty=allow_empty
        ):
            return serving.pprint(html=html, verbose=verbose, truncate=truncate), serving
        return f'{self.location.get_header(html=html)}\n\n{self.NO_MENU_FOUND_NOTE}', serving

    NO_MENU_FOUND_NOTE: ClassVar[str] = Serving.NO_MENU_FOUND_NOTE
    """
    :obj:`str`: Note indicating that no serving was found for this cafeteria.
    """
