#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module contains the OpeningHour class."""
import datetime as dtm
from functools import lru_cache, cached_property

from typing import Optional, ClassVar, List

from utils import TIMEZONE, JSONDict, TimesLiteral, parse_iso_time, setlocale
from .stwonobject import StwOnObject


class OpeningHour(StwOnObject):
    """
    Representation of an opening slot of a cafeteria.

    Args:
        time (:obj:`str`): Description of the serving time.
        start_day (:obj:`int`): 1-based weekday this serving time starts on.
        end_day (:obj:`int`): 1-based weekday this serving time ends on.
        start_time (:class:`datetime.time`): Starting time of the serving time.
        end_time (:class:`datetime.time`): Closing time of the serving time.

    Attributes:
        time (:obj:`str`): Description of the serving time.
        start_day (:obj:`int`): 1-based weekday this serving time starts on.
        end_day (:obj:`int`): 1-based weekday this serving time ends on.
        start_time (:class:`datetime.time`): Starting time of the serving time. Localized as
            :attr:`utils.TIMEZONE`.
        end_time (:class:`datetime.time`): Closing time of the serving time. Localized as
            :attr:`utils.TIMEZONE`.
    """

    MORNING: ClassVar[str] = 'morning'
    """:obj:`str`: Constant for morning servings."""
    NOON: ClassVar[str] = 'noon'
    """:obj:`str`: Constant for noon servings."""
    EVENING: ClassVar[str] = 'evening'
    """:obj:`str`: Constant for evening servings."""
    ALL: ClassVar[str] = 'all'
    """:obj:`str`: Constant for all servings."""
    ALL_TIMES: ClassVar[List[str]] = [MORNING, NOON, EVENING]
    """List[:obj:`str`]: List of all servings."""

    def __init__(
        self,
        time: TimesLiteral,
        start_day: int,
        end_day: int,
        start_time: dtm.time,
        end_time: dtm.time,
        **_kwargs: JSONDict,
    ) -> None:
        self.time = time
        self.start_day = int(start_day)
        self.end_day = int(end_day)
        self.start_time = start_time
        self.end_time = end_time

    @classmethod
    def de_json(cls, data: Optional[JSONDict]) -> Optional['OpeningHour']:
        data = cls.parse_data(data)

        if not data:
            return None

        data['start_time'] = parse_iso_time(data['start_time'])
        data['end_time'] = parse_iso_time(data['end_time'])

        return cls(**data)

    def to_dict(self) -> JSONDict:
        data = super().to_dict()

        data['start_time'] = self.start_time.isoformat()
        data['end_time'] = self.end_time.isoformat()

        return data

    @lru_cache(typed=True)
    def get_next_day(
        self,
        from_time: Optional[dtm.datetime] = None,
    ) -> Optional[dtm.date]:
        """
        Gets the next day with an opening slot. If there is an opening slot right now, this just
        gives the current day.

        Args:
            from_time (:obj:`datetime.datetime`, optional): If passed, will look for the next
                opening slot `after` this time. Defaults to :obj:`None`.

        Returns:
            The date with the next opening slot.
        """
        if from_time is None:
            from_time = dtm.datetime.now(tz=TIMEZONE)

        weekday = from_time.weekday() + 1  # python works 0-based
        if self.start_day <= weekday <= self.end_day:
            if from_time.time() < self.end_time:
                return from_time.date()
        next_day = (
            from_time
            + dtm.timedelta(days=1)
            - dtm.timedelta(hours=from_time.hour, minutes=from_time.minute)
        )
        return self.get_next_day(from_time=next_day)

    @lru_cache(typed=True)
    def covers(self, datetime: dtm.datetime, strict: bool = True) -> bool:
        """
        Checks if the opening slot is open at the specified date and time.

        Note:
            Naive datetime input will be assumed to be in :attr:`utils.TIMEZONE`.

        Args:
            datetime: The date to check.
            strict: If :obj:`False`, will also return :obj:`True`, if the specified datetime is
                *before* the opening slot, even though it hasn't yet started. It must still be on
                the same day. Default to :obj:`True`.

        """
        if datetime.tzinfo:
            effective_datetime = datetime
        else:
            effective_datetime = TIMEZONE.localize(datetime)

        weekday = effective_datetime.date().weekday() + 1  # python works 0-based
        if not self.start_day <= weekday <= self.end_day:
            return False
        if strict:
            if not self.start_time <= effective_datetime.timetz() < self.end_time:
                return False
            return True
        if effective_datetime.timetz() >= self.end_time:
            return False
        return True

    @cached_property
    def pprint(self) -> str:
        """
        Gives a human readable, german representation of this opening hours time slot, i.e. of
        :attr:`start_time` and :attr:`end_time`.

        Returns:
            :obj:`str`: The representation.
        """
        with setlocale('de_DE.UTF-8'):
            start_time = self.start_time.strftime('%H:%M')
            end_time = self.end_time.strftime('%H:%M')
            return f'{start_time} Uhr - {end_time} Uhr'
