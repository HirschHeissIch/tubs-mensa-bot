#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module contains the Price class."""
import locale
from functools import cached_property

from utils import setlocale, JSONDict

from .stwonobject import StwOnObject


class Price(StwOnObject):
    """
    Representation of the price of a meal. The string representation will be a pretty printed
    combination of :attr:`student`, :attr:`employee` and :attr:`guest` in that order.

    Args:
        student: The price for a student.
        employee: The price for a employee.
        guest: The price for a guest.

    Attributes:
        student (:obj:`float`): The price for a student.
        employee (:obj:`float`): The price for a employee.
        guest (:obj:`float`): The price for a guest.

    """

    def __init__(
        self,
        student: float,
        employee: float,
        guest: float,
        **_kwargs: JSONDict,
    ) -> None:
        self.student = float(student)
        self.employee = float(employee)
        self.guest = float(guest)

    @cached_property
    def pprint(self) -> str:
        """
        An easily readable representation of the price.

        Returns:
            :obj:`str`: The representation
        """
        with setlocale('de_DE.UTF-8'):
            prices = [
                locale.format_string('%.2f', p) for p in [self.student, self.employee, self.guest]
            ]
            return '{}€ / {}€ / {}€'.format(prices[0], prices[1], prices[2])
