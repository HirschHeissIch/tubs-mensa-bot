#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module contains some utility functionality for retrieving data from the API."""
import datetime as dtm
from typing import cast, Dict, Any, Tuple, List, Iterable, Union

import requests

from utils import JSONDict
from .meal import Meal
from .announcement import Announcement
from .openinghours import OpeningHour
from .cafeteria import Cafeteria

BRUNSWICK = [101, 105, 106, 120, 111, 109, 194]
"""List[:obj:`int`]: The IDs of the cafeterias located in Brunswick."""
API_URL = 'https://sls.api.stw-on.de/v1'
""":obj:`str`: The URL of the API of the Studentenwerk Ostniedersachsen."""


def __json_request(
    endpoint: str, params: Dict[str, str] = None
) -> Union[List[JSONDict], JSONDict]:
    return requests.get(f'{API_URL}/{endpoint}', params).json()


def get_cafeteria(cafeteria_id: int) -> Cafeteria:
    """
    Fetches information about the specified cafeteria from the API.

    Args:
        cafeteria_id (:obj:`int`): ID of the cafeteria to retrieve.

    Returns:
        :class:`stwon.Cafeteria`: The cafeteria.

    """
    return Cafeteria.de_json(  # type: ignore[return-value]
        cast(Dict[str, Any], __json_request(f'location/{cafeteria_id}'))
    )


def get_cafeterias(ids: Iterable[int] = None) -> List[Cafeteria]:
    """
    Fetches information about the specified cafeterias from the API.

    Args:
        ids (List[:obj:`int`], optional): The IDs of the cafeterias to get. Defaults to
            :attr:`BRUNSWICK`.

    Returns:
        List[:class:`stwon.Cafeteria`]: The cafeterias
    """
    if not ids:
        ids = BRUNSWICK
    result = cast(List[Dict[str, Any]], __json_request('location'))
    cafeterias = Cafeteria.de_list(result)
    effective_cafeterias = (caf for caf in cafeterias if caf is not None)
    return [c for c in effective_cafeterias if c.id in ids]


def get_meals(  # pylint: disable=C0103
    id: int,  # pylint: disable=W0622
    start_date: dtm.date = None,
    end_date: dtm.date = None,
    time: str = None,
) -> Tuple[List[Announcement], List[Meal]]:
    """
    Fetches information about announcements and meals for the specified cafeteria.

    Args:
        id (:obj:`int`): ID of the cafeteria.
        start_date (:obj:`datetime.date`, optional): First date to fetch meals for. Defaults to
            :meth:`datetime.date.today()`
        end_date: (:obj:`datetime.date`, optional): Last date to fetch meals for. Must be within
            21 days from :attr:`start_date`. Defaults to :attr:`start_date` + 21 days.
        time (:obj:`str`, optional): The serving time to fetch menus for. Must be one of
            :attr:`stwon.OpeningHour.ALL_TIMES` or :attr:`stwon.OpeningHour.ALL`. Defaults to
            :attr:`stwon.OpeningHour.ALL`.

    Returns:
        (List[:class:`stwon.Announcement`], List[:class:`stwon.Meal`]): Announcements and meals.
    """
    if not start_date:
        start_date = dtm.date.today()
    if not end_date:
        end_date = start_date + dtm.timedelta(days=21)
    if not time:
        time = OpeningHour.ALL

    result = __json_request(
        f'location/{id}/menu/{start_date.isoformat()}/{end_date.isoformat()}',
        params={'time': time},
    )
    result = cast(Dict[str, Any], result)

    announcements = Announcement.de_list(result.get('announcements'))
    meals = Meal.de_list(result.get('meals'))

    effective_announcements = [an for an in announcements if an is not None]
    effective_meals = [ml for ml in meals if ml is not None]

    return effective_announcements, effective_meals


def next_cafeteria(c_id: int, reverse: bool = False) -> int:
    """
    Gives the ID of the next cafeteria in :attr:`BRUNSWICK`.

    Args:
        c_id: The ID of the currently considered cafeteria.
        reverse: Optional. If :obj:`True`, gives the previous cafeteria. Defaults to :obj:`False`.
    """
    idx = BRUNSWICK.index(c_id)
    if reverse:
        return BRUNSWICK[(idx - 1) % len(BRUNSWICK)]
    return BRUNSWICK[(idx + 1) % len(BRUNSWICK)]
