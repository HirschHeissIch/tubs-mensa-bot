#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module contains the Tags class."""
from functools import lru_cache
from typing import List, Optional

from utils import JSONDict
from .stwonobject import StwOnObject
from .meallabel import MealLabel


class Tags(StwOnObject):
    """
    Representation of a collection of tags for a :class:`stwon.Meal`.

    Args:
        categories (List[:class:`stwon.MealLabel`]): General categories.
        allergens (List[:class:`stwon.MealLabel`]): Contained allergens
        additives (List[:class:`stwon.MealLabel`]): Contained additives
        special (List[:class:`stwon.MealLabel`]): Special tags.

    Attributes:
        categories (List[:class:`stwon.MealLabel`]): General categories.
        allergens (List[:class:`stwon.MealLabel`]): Contained allergens
        additives (List[:class:`stwon.MealLabel`]): Contained additives
        special (List[:class:`stwon.MealLabel`]): Special tags.
    """

    def __init__(
        self,
        categories: List[MealLabel],
        allergens: List[MealLabel],
        additives: List[MealLabel],
        special: List[MealLabel],
        **_kwargs: JSONDict,
    ) -> None:
        self.categories = categories
        self.allergens = allergens
        self.additives = additives
        self.special = special

    @classmethod
    def de_json(cls, data: Optional[JSONDict]) -> Optional['Tags']:
        data = cls.parse_data(data)

        if not data:
            return None

        data['categories'] = MealLabel.de_list(data.get('categories'))
        data['allergens'] = MealLabel.de_list(data.get('allergens'))
        data['additives'] = MealLabel.de_list(data.get('additives'))
        data['special'] = MealLabel.de_list(data.get('special'))

        return cls(**data)

    def to_dict(self) -> JSONDict:
        data = super().to_dict()

        data['categories'] = [category.to_dict() for category in self.categories]
        data['allergens'] = [allergen.to_dict() for allergen in self.allergens]
        data['additives'] = [additive.to_dict() for additive in self.additives]
        data['special'] = [special.to_dict() for special in self.special]

        return data

    @lru_cache(typed=True)
    def pprint_categories(self, multiline: bool = False, truncate: int = 0) -> str:
        """
        Gives a human readable list of the short representations of the categories and their
        long name.

        Args:
            multiline (:obj:`bool`, optional): Whether each category should get its own line.
                Defaults to :obj:`False`.
            truncate (:obj:`int`, optional): Maximum length of the result.
                 '...' be appended, if the result needs truncation. Truncation happens
                 categories meals. Pass ``0`` to not truncate. Defaults to ``0``.

        """
        cats = (f'{cat.pprint} - {cat.name}' for cat in self.categories)
        sep = '\n' if multiline else ', '
        result = sep.join(cats)

        if not truncate:
            return result

        if len(result) > truncate:
            note = f'{sep}{self._TRUNCATION_NOTE}'
            count = 0

            while len(f'{result}{note}') > truncate:
                count += 1
                temp, _, _ = result.rpartition(sep)
                result = temp

            result = f'{result}{note}'

        return result

    _TRUNCATION_NOTE = '...'
