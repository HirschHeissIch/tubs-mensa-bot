#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module contains the Cafeteria class."""
from functools import lru_cache
from typing import List, Dict, Optional, ClassVar

from telegram import InlineKeyboardButton

from utils import JSONDict, TimesLiteral
from .address import Address
from .stwonobject import StwOnObject
from .openinghours import OpeningHour


class Cafeteria(StwOnObject):
    """
    Representation of a cafeteria of the Studentenwerk Ostniedersachsen.

    Objects of this class are comparable in terms of equality. Two objects of this class are
    considered equal, if their :attr:`id` is equal.

    Args:
        id (:obj:`int`): The ID of the cafeteria.
        name (:obj:`str`): The name of the cafeteria.
        address (:class:`stwon.Address`): The address of the cafeteria.
        opening_hours (List[:class:`stwon.OpeningHour`]): List of opening hours.

    Attributes:
        id (:obj:`int`): The ID of the cafeteria.
        name (:obj:`str`): The name of the cafeteria.
        address (:class:`stwon.Address`): The address of the cafeteria.
        opening_hours (List[:class:`stwon.OpeningHour`]): List of opening hours.
    """

    _URLS: ClassVar[Dict[int, str]] = {
        101: 'https://www.stw-on.de/braunschweig/essen/mensen-cafeterien/mensa-1/',
        105: 'https://www.stw-on.de/braunschweig/essen/mensen-cafeterien/mensa-2/',
        106: 'https://www.stw-on.de/braunschweig/essen/mensen-cafeterien/cafeteria-mensa-2/',
        120: 'https://www.stw-on.de/braunschweig/essen/mensen-cafeterien/kantina-kreativa/',
        111: 'https://www.stw-on.de/braunschweig/essen/mensen-cafeterien/360/',
        109: 'https://www.stw-on.de/braunschweig/essen/mensen-cafeterien/bistro4u/',
        194: 'https://www.stw-on.de/braunschweig/essen/mensen-cafeterien/foodtruck/',
    }

    def __init__(
        self,
        id: int,  # pylint: disable=W0622
        name: str,
        address: Address,
        opening_hours: List[OpeningHour],
        **_kwargs: JSONDict,
    ) -> None:
        self.id = int(id)  # pylint: disable=C0103
        self.name = name
        self.address = address
        self.opening_hours = opening_hours

        self._id_attrs = (self.id,)

    @classmethod
    def de_json(cls, data: Optional[JSONDict]) -> Optional['Cafeteria']:
        data = cls.parse_data(data)

        if not data:
            return None

        data['address'] = Address.de_json(data.get('address'))
        data['opening_hours'] = OpeningHour.de_list(data.get('opening_hours'))

        return cls(**data)

    def to_dict(self) -> JSONDict:
        data = super().to_dict()

        data['opening_hours'] = [o_h.to_dict() for o_h in self.opening_hours]

        return data

    @property
    def url(self) -> str:
        """
        URL of the online menu of the cafeteria.
        """
        return self._URLS.get(
            self.id, 'https://www.stw-on.de/braunschweig/essen/mensen-cafeterien'
        )

    @property
    def short_name(self) -> str:
        """
        Tries to give a short representation of :attr:`name`. Currently this is done by stripping
        ``Braunschweig``, ``TU Braunschweig`` and ``TU BS`` from :attr:`name`.

        Returns:
            The short name.
        """
        return (
            self.name.replace(' TU Braunschweig', '')
            .replace(' TU BS', '')
            .replace(' Braunschweig', '')
        )

    @lru_cache(typed=True)
    def get_opening_hour(self, time: TimesLiteral) -> Optional[OpeningHour]:
        """
        Gives the cafeterias opening hour for the specified time, if it exists.

        Args:
            time: The time.

        """
        return next((o_h for o_h in self.opening_hours if o_h.time == time), None)

    @lru_cache(typed=True)
    def get_header(self, html: bool = False, closed: bool = False) -> str:
        """
        Gives a string that can be used as header for menus of this cafeteria.

        Args:
            html: If :obj:`True`, will contain HTML code as expected by
                :attr:`telegram.Bot.send_message`. Defaults to :obj:`False`.
            closed: Whether the cafeteria is currently closed.

        """
        if closed:
            name = f'{self.name} ⛔️ Geschlossen'
        else:
            name = self.name
        return name if not html else f'<b>{name}</b>'

    @property
    def menu_button(self) -> InlineKeyboardButton:
        """
        An :class:`telegram.InlineKeyobardbutton` leading to the menu of this cafeteria.
        """
        return InlineKeyboardButton(
            'Online-Speiseplan 🍽',
            url=self.url,
        )
