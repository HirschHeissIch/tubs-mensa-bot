#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module contains the Address class."""
import datetime as dtm
from functools import cached_property, lru_cache
from typing import Optional, Union, List

from utils import JSONDict, TimesLiteral
from .stwonobject import StwOnObject
from .openinghours import OpeningHour


class Announcement(StwOnObject):
    """
    Representation of an announcement of a cafeteria of the Studentenwerk Ostniedersachsen.

    Objects of this class are comparable in terms of equality. Two objects of this class are
    considered equal, if their :attr:`id` is equal.

    Args:
        id (:obj:`int`): The ID of the announcement
        start_date (:obj:`datetime.date`): Date the announcement starts taking effect.
        end_date (:obj:`datetime.date`): Date the announcement ends taking effect.
        text (:obj:`str`): The actual announcement.
        closed (:obj:`bool`): Whether the cafeteria is closed while this announcement is in action.
        time (:obj:`str` | List[:obj:`str`): Serving time(s) this announcement is valid for.

    Attributes:
        id (:obj:`int`): The ID of the announcement
        start_date (:obj:`datetime.date`): Date the announcement starts taking effect.
        end_date (:obj:`datetime.date`): Date the announcement ends taking effect.
        text (:obj:`str`): The actual announcement.
        closed (:obj:`bool`): Whether the cafeteria is closed while this announcement is in action.
        time (:obj:`str` | List[:obj:`str`): Serving time(s) this announcement is valid for.

    """

    def __init__(
        self,
        id: int,  # pylint: disable=W0622
        start_date: dtm.date,
        end_date: dtm.date,
        text: str,
        closed: bool,
        time: Union[List[str], str],
        **_kwargs: JSONDict,
    ) -> None:
        self.id = int(id)  # pylint: disable=C0103
        self.start_date = start_date
        self.end_date = end_date
        self.text = text
        self.closed = closed
        self.time = time

        self._id_attrs = (self.id,)

    @cached_property
    def times(self) -> List[str]:
        """
        Convenience property.

        Returns:
            List[:obj:`str`]: List of effectively affected serving times.
        """
        if self.time == OpeningHour.ALL:
            return OpeningHour.ALL_TIMES.copy()
        if isinstance(self.time, str):
            return [self.time]
        return self.time

    @classmethod
    def de_json(cls, data: Optional[JSONDict]) -> Optional['Announcement']:
        data = cls.parse_data(data)

        if not data:
            return None

        data['start_date'] = dtm.date.fromisoformat(data['start_date'])
        data['end_date'] = dtm.date.fromisoformat(data['end_date'])

        return cls(**data)

    def to_dict(self) -> JSONDict:
        data = super().to_dict()

        data['start_time'] = self.start_date.isoformat()
        data['end_time'] = self.end_date.isoformat()

        return data

    @lru_cache(typed=True)
    def covers(self, date: dtm.date, time: TimesLiteral) -> bool:
        """
        Checks if the announcement is relevant for the serving specified by :attr:`date` and
        :attr:`time`.

        Args:
            date: The date to check.
            time: The serving time to check.

        """
        if not self.start_date <= date <= self.end_date:
            return False
        if time not in self.times:
            return False
        return True
