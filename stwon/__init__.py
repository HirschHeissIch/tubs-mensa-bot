#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Objects representing the menu of cafeterias of the Studentenwerk Ostniedersachsen."""
from .stwonobject import StwOnObject
from .address import Address
from .announcement import Announcement
from .cafeteria import Cafeteria
from .lane import Lane
from .meal import Meal
from .meallabel import MealLabel
from .openinghours import OpeningHour
from .price import Price
from .serving import Serving
from .servingscollection import ServingsCollection
from .tags import Tags
from .utils import API_URL, BRUNSWICK, get_cafeteria, get_cafeterias, get_meals, next_cafeteria

__all__ = (
    'Address',
    'Announcement',
    'Cafeteria',
    'OpeningHour',
    'Lane',
    'Meal',
    'MealLabel',
    'Price',
    'Serving',
    'ServingsCollection',
    'StwOnObject',
    'Tags',
    'BRUNSWICK',
    'API_URL',
    'get_meals',
    'get_cafeterias',
    'get_cafeteria',
    'next_cafeteria',
)
